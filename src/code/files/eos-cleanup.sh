#!/bin/bash
#
# This script will find eosd mounts with no eosxd fuse process present
# and unmount them.
#
# Author: EOS team
# Date: 2024
#

export PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin

# Wrap cleanup procedures under a lock on FD 9, not to run the script more than once
(
        if ! flock -n 9; then
                echo "Still running this script... terminating" 2>&1
                exit 1
        fi

	# 1) First test was to find stuck eosxd, i.e. eosxd without /dev/fuse open
        #    (such cases attributed to https://github.com/xrootd/xrootd/issues/418).
        #    However the test was matching spurious processes and also
        #    apparently no longer finding any stuck eosxd. Therefore this
        #    test is no longer done.

	# 2) Find mounts in /eos/* which do not have a corresponding eosxd
	# ONLY IF AUTOFS!!
	if [ -f /etc/auto.eos ]
	then
		# eosxd mounts have type fuse and are mounted in /eos/*
		EOSXD_MOUNTS=$(grep -E '\bfuse\b' /proc/mounts | grep -v '^eos' | awk '{print $2}' | grep '^/eos/')
		for MOUNT in $EOSXD_MOUNTS
		do
			EOSXD=$(pgrep -u root -f "eosxd ${MOUNT}")
			if [[ -z "${EOSXD}" ]]
			then
				logger -t eos-cleanup.sh "${MOUNT} is mounted but corresponding eosxd is not running. Unmounting."
				umount -l "${MOUNT}"
			fi
		done
	fi

	# 3) Find mounts in /eos/* which do not have a corresponding eosxd
	# Roberto V: ONLY USING NEW CONFIG LAYOUT
	if [[ -f /etc/auto.master.d/eos.misc.lxplus.autofs || -f /etc/auto.master.d/eos.user.lxplus.autofs || -f /etc/auto.master.d/eos.project.lxplus.autofs ]]
	then
			# eosxd mounts have type fuse and are mounted in /eos/*
			EOSXD_MOUNTS=$(grep -E '\bfuse\b' /proc/mounts | grep -v '^eos' | awk '{print $2}' | grep '^/eos/' | grep -vE 'home|project\-')
			for MOUNT in $EOSXD_MOUNTS
			do
					EOSXD=$(pgrep -u root -f "eosxd ${MOUNT}")
					if [[ -z "${EOSXD}" ]]
					then
							logger -t eos-cleanup.sh "${MOUNT} is mounted but corresponding eosxd is not running. Unmounting."
							umount -l "${MOUNT}"
					fi
			done
	fi

) 9> /var/run/eos-cleanup.lock
