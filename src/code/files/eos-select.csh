# guess the EOS instance to talk to, based on UNIX group
# CSH

# not found: user will need to explicitly set EOS_MGM_URL
#     or this file needs updating
# Do not print anything here (might mess with non-interactive shells)

if ( $?EOS_MGM_URL ) then
    exit # works on tcsh
endif

if ( !  $?GROUP ) then 
    set GROUP `id -gn`
endif

# only look at specific Experiment GROUPs
switch ("$GROUP" )
    case "zp":
	setenv EOS_MGM_URL "root://eosatlas.cern.ch"
	breaksw
    case "zh":
	setenv EOS_MGM_URL "root://eoscms.cern.ch"
	breaksw
    case "c3":
	setenv EOS_MGM_URL "root://eospps.cern.ch"
	breaksw
    case "z5":
	setenv EOS_MGM_URL "root://eoslhcb.cern.ch"
	breaksw
    case "va":
	setenv EOS_MGM_URL "root://eosams.cern.ch"
	breaksw
    case "xv":
	setenv EOS_MGM_URL "root://eosams.cern.ch"
	breaksw
    case "xu":
        setenv EOS_MGM_URL "root://eospublic.cern.ch"
	breaksw
    case "vy":
	setenv EOS_MGM_URL "root://eoscompass.cern.ch"
	breaksw
    case "vl":
	setenv EOS_MGM_URL "root://eosna62.cern.ch"
	breaksw
    case "wj":
	setenv EOS_MGM_URL "root://eosna61.cern.ch"
	breaksw
    case "t3":
	setenv EOS_MGM_URL "root://eospublic.cern.ch"
	breaksw
    case "zj":
	setenv EOS_MGM_URL "root://eostotem.cern.ch"
	breaksw
    case "vp":
	setenv EOS_MGM_URL "root://eospublic.cern.ch"
	breaksw
    case "ed":
        setenv EOS_MGM_URL "root://eospublic.cern.ch"
	breaksw
    case "ship-cg":
	setenv EOS_MGM_URL "root://eospublic.cern.ch"
	breaksw
endsw

