# guess the EOS instance to talk to, based on UNIX group
# SH

# no mapping: user will need to explicitly set EOS_MGM_URL
#     or this file needs updating
# Do not print anything here (might mess with non-interactive shells),
#   nor call exit() - this is sourced, so would kick out users.

if [ -z "$GROUP" ]; then 
    GROUP=`id -gn`
fi
if [ -z "$EOS_MGM_URL" ]; then
    # only look at specific Experiment GROUPs
    if   [ "$GROUP" = "zp" ]; then
	export EOS_MGM_URL="root://eosatlas.cern.ch"
    elif [ "$GROUP" = "zh" ]; then
	export EOS_MGM_URL="root://eoscms.cern.ch"
    elif [ "$GROUP" = "z5" ]; then
	export EOS_MGM_URL="root://eoslhcb.cern.ch"
    elif [ "$GROUP" = "va" -o "$GROUP" = "xv" ]; then
	export EOS_MGM_URL="root://eosams.cern.ch"
    elif [ "$GROUP" = "xu" ]; then
        export EOS_MGM_URL="root://eospublic.cern.ch"
    elif [ "$GROUP" = "vy" ]; then
	export EOS_MGM_URL="root://eoscompass.cern.ch"
    elif [ "$GROUP" = "vl" ]; then
	export EOS_MGM_URL="root://eosna62.cern.ch"
    elif [ "$GROUP" = "wj" ]; then
	export EOS_MGM_URL="root://eosna61.cern.ch"
    elif [ "$GROUP" = "t3" ]; then
	export EOS_MGM_URL="root://eospublic.cern.ch"
    elif [ "$GROUP" = "zj" ]; then
	export EOS_MGM_URL="root://eostotem.cern.ch"
    elif [ "$GROUP" = "vp" ]; then
	export EOS_MGM_URL="root://eospublic.cern.ch"
    elif [ "$GROUP" = "ed" ]; then
        export EOS_MGM_URL="root://eospublic.cern.ch"
    elif [ "$GROUP" = "ship-cg" ]; then
	export EOS_MGM_URL="root://eospublic.cern.ch"
    fi
fi
