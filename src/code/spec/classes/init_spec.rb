# frozen_string_literal: true

require 'spec_helper'
describe 'eosclient' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      context 'with default parameters' do
        it { is_expected.to compile.with_all_deps }

        it { is_expected.to have_eosclient__mount_resource_count(0) }
        it { is_expected.to contain_class('eosclient') }
        it { is_expected.to contain_package('eos-client').with_ensure('installed') }
        it { is_expected.to contain_package('eos-debuginfo').with_ensure('absent') }

        case facts[:os]['release']['major']
        when '7'
          context 'when on CentOS7' do
            it { is_expected.to contain_package('eos-xrootd-debuginfo').with_ensure('installed') }
            it { is_expected.to contain_package('eos-protobuf3-debuginfo').with_ensure('installed') }
          end
        else
          context 'when on C8 or newer' do
            it { is_expected.to contain_package('eos-xrootd-debuginfo').with_ensure('installed') }
            it { is_expected.not_to contain_package('eos-protobuf3-debuginfo').with_ensure('installed') }
          end
        end

        context 'when on aarch64 architecture' do
          let(:facts) do
            facts.merge(os: { architecture: 'aarch64', release: { major: facts[:os]['release']['major'] } })
          end

          case facts[:os]['release']['major']
          when '7'
            context 'when on CentOS7' do
              it { is_expected.not_to compile }
            end
          else
            context 'when on CentOS8 or newer' do
              it { is_expected.to compile }
            end
          end
        end
      end

      context 'with version parameter set' do
        let(:params) do
          { version: '1.2.3',
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('eos-client').with_ensure('1.2.3') }
      end

      context 'with enable_fuse parameter set' do
        let(:params) do
          { enable_fuse: true,
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('eos-fuse') }
        it { is_expected.to contain_service('autofs') }

        case facts[:os]['release']['major']
        when '7'
          it { is_expected.to contain_augeas('eos_automaster') }
          it { is_expected.not_to contain_file('/etc/auto.master.d/eos.autofs') }
        else
          it { is_expected.not_to contain_augeas('eos_automaster') }
          it { is_expected.to contain_file('/etc/auto.master.d/eos.autofs').with_content(%r{^/eos /etc/auto.eos$}) }
        end
      end

      context 'with enable_fuse, some (non-default) mounts set' do
        let(:params) do
          { enable_fuse: true,
            mounts: %w[atlas home-a],
            default_mounts: %w[ams home-b] }
        end

        it { is_expected.to compile.with_all_deps }

        # things that should be configured
        it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{hostport}) } # new FUSEX
        # things that should not be configured
        it { is_expected.to contain_file('/etc/sysconfig/eos.ams').with_ensure('absent') }     # old FUSE
        it { is_expected.to contain_file('/etc/eos/fuse.home-b.conf').with_ensure('absent') }  # new FUSEX
      end

      context 'with enable_fuse , no mounts set (use default)' do
        let(:params) do
          { enable_fuse: true,
            default_mounts: ['home-b'] }
        end

        # things that should be configured
        it { is_expected.to contain_file('/etc/eos/fuse.home-b.conf').with_content(%r{hostport}) } # new FUSEX
        # things that should not be configured
        it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_ensure('absent') } # new FUSEX
      end

      context 'with enable_autofs, and with enable_fuse parameter set' do
        let(:params) do
          { enable_fuse: true,
            enable_autofs: true,
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('eos-fuse') }
        it { is_expected.to contain_service('autofs') }
      end

      context 'with enable_autofs, manage_autofs_service=>false and with enable_fuse parameter set' do
        let(:params) do
          { enable_fuse: true,
            enable_autofs: true,
            manage_autofs_service: false,
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('eos-fuse') }
        it { is_expected.not_to contain_service('autofs') }
      end

      context 'with enable_pam_hook parameter set' do
        let(:params) do
          { enable_pam_hook: true,
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_file('/etc/profile.d/eos-select.sh') }
      end

      context 'with fuse true and with mounts parameter set' do
        let(:params) do
          { enable_fuse: true,
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_file('/etc/eos/fuse.atlas.conf') }
        it { is_expected.to contain_file('/etc/sysconfig/eos.atlas').with_ensure('absent') }
      end

      context 'with fuse true and deprecated hiera variables' do
        let(:params) do
          { enable_fuse: true,
            eosxd_ready: [],
            mounts: %w[atlas cms home-a] }
        end

        it { is_expected.not_to compile }
        it { is_expected.not_to compile.and_raise_error(%r{The hiera key eosclient::eosxd_ready is gone please remove it, EOS instances are mounted with eosxd/}) }
      end

      context 'with fuse enabled, common_config, with mounts parameter set' do
        let(:params) do
          { enable_fuse: true,
            mounts: %w[atlas cms home-a],
            common_config: { 'A' => 'B',
                             'conf' => { 'a' => 'b' } },
            instance_config: { 'home-a' => { 'conf' => { 'cache' => { 'size-mb' => '1234' } } },
                               'atlas' => { 'C' => 'D' },
                               'cms' => { 'E' => 'F' } } }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_eosclient__mount('atlas') }
        it { is_expected.to contain_eosclient__mount('home-a') }
        it { is_expected.to contain_eosclient__conf__eosxd('atlas') }
        it { is_expected.to contain_eosclient__conf__eosxd('home-a') }
        it { is_expected.not_to contain_file('/etc/sysconfig/eos') }
        it { is_expected.to contain_file('/etc/sysconfig/eos.atlas').with_ensure('absent') }
        it { is_expected.to contain_file('/etc/sysconfig/eos.home-a').with_ensure('absent') }
        it { is_expected.to contain_file('/etc/sysconfig/eos.cms').with_ensure('absent') }
        it { is_expected.to contain_file('/etc/eos/fuse.atlas.conf').without_content(%r{"C":"D"}) }
        it { is_expected.to contain_file('/etc/eos/fuse.atlas.conf').with_content(%r{^null}) }
        it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"size-mb":"1234"}) }
        it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"a":"b"}) }
        it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').without_content(%r{"A"}) }

        context 'with deprecated hiera variables' do
          let(:hiera_config) { 'spec/hiera.yaml' }

          it { is_expected.to contain_eosclient__mount('atlas') }
          it { is_expected.to contain_eosclient__mount('home-a') }
          it { is_expected.to contain_eosclient__conf__eosxd('atlas') }
          it { is_expected.to contain_eosclient__conf__eosxd('home-a') }
          it { is_expected.not_to contain_file('/etc/sysconfig/eos') }
          it { is_expected.to contain_file('/etc/sysconfig/eos.atlas').with_ensure('absent') }
          it { is_expected.to contain_file('/etc/sysconfig/eos.home-a').with_ensure('absent') }
          it { is_expected.to contain_file('/etc/sysconfig/eos.cms').with_ensure('absent') }
          it { is_expected.not_to contain_file('/etc/eos/fuse.atlas.conf').with_ensure('absent') }
          it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"size-mb":"1234"}) }
          it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"a":"b"}) }
          it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').without_content(%r{"A"}) }
          it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"eta":"theta"}) }
          it { is_expected.to contain_file('/etc/eos/fuse.home-a.conf').with_content(%r{"lamda":"mu"}) }
        end

        # Remove this but for the moment send a warning to remove this configuration
        context 'with nonempty fuse_logrotate_size parameter set, fusex enabled' do
          let(:params) do
            { fuse_logrotate_size: '123M',
              fuse_logrotate_days: 7,
              enable_fuse: true,
              mounts: %w[atlas cms home-a] }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_augeas('eosfusex_logrotate') }

          it do
            is_expected.to contain_augeas('eosfusex_logrotate').with(
              changes: ['set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate 7',
                        'set /files/etc/logrotate.d/eos-fusex-logs/rule/size 123M']
            )
          end
        end

        context 'with fuse_logrotate_size parameter unset, fusex enabled' do
          let(:params) do
            { fuse_logrotate_days: 7,
              enable_fuse: true,
              mounts: %w[atlas cms home-a] }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_augeas('eosfusex_logrotate') }

          it do
            is_expected.to contain_augeas('eosfusex_logrotate').with(
              changes: ['set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate 7',
                        'rm /files/etc/logrotate.d/eos-fusex-logs/rule/size']
            )
          end
        end
      end

      context 'with configure_via_rpm parameter set' do
        let(:params) do
          { configure_via_rpm: true }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('cern-eos-autofs-lxplus').with_ensure('present') }

        # old stuff that should be gone. Eventually remove from profiles and change below to 'is_expected.not_to'
        it { is_expected.to contain_package('eos-fuse').with_ensure('absent') }
        it { is_expected.to contain_package('eos-fuse-sysv').with_ensure('absent') }
        it { is_expected.to contain_file('/etc/auto.eos').with_ensure('absent') }
      end

      context 'with configure_via_rpm parameter, enable_squashfs set' do
        let(:params) do
          { configure_via_rpm: true,
            enable_squashfs: true }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('cern-eos-autofs-lxplus').with_ensure('present') }
        it { is_expected.to contain_package('cern-eos-autofs-squashfs').with_ensure('present') }
      end

      context 'with use_osrepos turned off' do
        let(:params) do
          { use_osrepos: false }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.not_to contain_osrepos__kojitag('eos') }
      end
    end
  end
end
