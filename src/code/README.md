# Module 'eosclient'

This module configures an EOS client and supports CC7 and above. The
module can optionally FUSE mount /eos and hook krb5 credentials to PAM.

Regarding EOS FUSE mounts, the module supports `eosxd` (aka FUSEX).

## Basic Usage:

To install the EOS client RPMs (and not configure any /eos mounts):

```puppet
include eosclient
```

## Configuring /eos mounts:

To configure and also enable /eos mounts, use yaml:

```YAML
---
eosclient::enable_fuse: true
```

To specify which EOS instances to mount, set the ``mounts`` to a list of
valid EOS instance names, e.g. ``['user','atlas']``.

## Parameters to Eosclient Class
(see directly in `manifests/init.pp`)

Puppet databindings allow all the above settings to be set via hiera:

```yaml
---
eosclient::enable_fuse: true
eosclient::enable_pam_hook: false
eosclient::fuse_logrotate_days: 2
eosclient::fuse_logrotate_size: '250M'  
```

## /eos Configuration Options (FUSEX)
This module uses the default eosxd configution options, documented here:
https://gitlab.cern.ch/dss/eos/blob/master/fusex/README.md

If you would like to override a setting for *all* eosxd mounts on your
system, create a hiera hash called `eosclient::common_config`. For
example, custom cache options may be configured as follows:

```yaml
---
eosclient::common_config:
  conf:
    cache:
      size-mb: 1024
```

You are also able to override settings for just a subset of eos mounts with
the following syntax:

```yaml
---
eosclient::instance_config:
  home-a:
    conf:
      cache:
        size-mb: 1024
```

## Deprecations
These hiera variables are deprecated but still work. Please migrate.
* `eosclient_instance_config` becomes `eosclient::instance_config`.
* `eosclient_custom_config` becomes `eosclient::common_config`.

The following hiera variables are deprecated and will cause a failure if they are
set.
* `eos_mounts` became `eosclient::mounts`
* `eosclient_common_config` became `eosclient::common_config`.
* `eosclient_eosxd_ready_instances` became `eosclient::eosxd_ready` (also deprecated, see above).
* `eosclient::eosxd_ready` should not be set - all EOS instances will by default be accessed via `eosxd`


## Example: Auto-mounted /eos with defaults (like on LXPLUS)
This example shows how to setup the module with auto-mounted /eos, similar to
what is done on LXPLUS, and uses the default list of EOS mounts. This is the recommended configuration.

```puppet
include eosclient
```

```yaml
---
eosclient::enable_fuse: true
```

## Other examples

```puppet
include eosclient
```
To mount ONLY storage spaces for users whose username starts with letter _h_ :

```yaml
---
eosclient::enable_fuse: true
eosclient::mounts:
  - user
  - home-h
```

To mount ONLY a project space that starts with letter _c_ :

```yaml
---
eosclient::enable_fuse: true
eosclient::mounts:
  - project
  - project-c
```

## Simplified configuration via RPM

It is possible to have a simplified configuration based on rpm which will configure all home/project/physics 
fuse mounts automatically and managed by autofs. 

```yaml
eosclient::configure_via_rpm: true
```

>NOTE: Enabling this feature will ignore any other custom eosclient configuration.

## Enable SquashFs support

While using configuration via rpm, is possible to enable SquashFs support. EOS-squashfs will allow you to create
read-only software image packages in the EOS area which will be mounted automatically by autofs when accessed via fuse.

```yaml
eosclient::enable_squashfs: true
```
> NOTE: At the time being, only physics eos areas are squashFs-enabled. 

### EOS Squashfs Howto
#### Enabling software packages / squash images

- Create a software directory on EOS

```
eos mkdir -p /eos/instance/f/foo/software/
```

- Share the software directory for read-only with *eosnobody* updating the folder acls. 

#### Create you first software package/image

```
mkdir /eos/instance/f/foo/software/kernel/

# create a new software package called 'git'
eos squash new /eos/instance/f/foo/software/kernel/git

# checkout the kernel git repository into this
cd /eos/instance/f/foo/software/kernel/git
git clone https://github.com/torvalds/linux.git
cd -

# close the image
eos squash pack /eos/instance/f/foo/software/kernel/git
```

#### Use the software package

When you access the software package path via fuse, the cern-eos-autofs-squashfs automount configuration will mount the image file automatically.


#### Other useful commands

```
# info command - tells you if image is packed or unpacked 
eos squash info /eos/instance/f/foo/software/kernel/git

# rm command - remove a squash fs image
eos squash rm /eos/instance/f/foo/software/kernel/git

# install command - install a *.tar.gz file as an image
eos squash install --curl=https://<package>.tgz|.tar.gz <path>

# relabel command - if an image has been moved in the namespace, it has to be relabeld

# roll/unroll (currently not implemented)
```