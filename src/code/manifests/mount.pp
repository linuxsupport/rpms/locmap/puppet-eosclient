# @summary writes EOS per-mountpoint config
#  - for FUSEX, via conf::eosxd
#  will be instantiated several times (once per mountpoint)
#
# @param config
#  Hiera dict with 'conf' dict (for FUSEX)
# @param instance
#  client-local name of the mountpoint
# @param remove
#  if set, just remove the on-disk configuration for this mount

define eosclient::mount (
  Optional[Hash] $config = undef,
  String         $instance = $name,
  Boolean        $remove   = false,
  ) {
  file{"/etc/sysconfig/eos.${instance}":
    ensure => 'absent',
  }

  if $remove {
    file{"/etc/eos/fuse.${instance}.conf":
      ensure => 'absent',
    }
  } else {
    ::eosclient::conf::eosxd{ $instance:
      config => $config,
    }
    file{"/var/log/eos/fuse/fuse.${instance}.log":
      ensure => file,
      mode   => '0600',
      owner  => root,
      group  => root,
    }
  }
}
