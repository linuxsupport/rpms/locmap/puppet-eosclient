class eosclient::fuse {

  # repo set up etc.
  # keep this include until all clients drop the deprecated include eosclient::fuse syntax
  include eosclient

  # To support downgrade from new mount layout

  stdlib::ensure_packages('cern-eos-autofs-lxplus', {'ensure' => 'absent'})
  stdlib::ensure_packages('cern-eos-autofs-squashfs', {'ensure' => 'absent'})

  $allmounts = keys($eosclient::instance_config)

  if empty($eosclient::mounts) {
    $eosmounts = $eosclient::default_mounts
  } else {
    $eosmounts = $eosclient::mounts
  }

  $unmounted = difference($allmounts, $eosmounts)
  eosclient::mount { $unmounted:
    remove => true,
  }

  $eospkg = ['eos-fusex']

  if $eosclient::use_osrepos {
    Osrepos::Kojitag['eos'] -> Package[$eospkg]
  }

  stdlib::ensure_packages($eospkg, {'ensure' => $eosclient::version})

  # rotating by 'size' implictly turns off 'daily' rotation.
  # The augeas lens does something funny with the "daily" entry, don't touch.
  $logrotate_fusex_changes = $eosclient::fuse_logrotate_size ? {
    undef   => [
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate ${eosclient::fuse_logrotate_days}",
                  'rm /files/etc/logrotate.d/eos-fusex-logs/rule/size',
                ],
    default => [
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate ${eosclient::fuse_logrotate_days}",
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/size ${eosclient::fuse_logrotate_size}",
                ],
  }

  augeas{'eosfusex_logrotate':
    changes => $logrotate_fusex_changes,
    incl    => '/etc/logrotate.d/eos-fusex-logs',
    lens    => 'Logrotate.lns',
    require =>  Package[$eospkg],
  }
  -> file{'/eos':
    ensure  => directory,
    mode    => '0755',
    owner   => root,
    group   => root,
    seltype => 'default_t',
  }
  -> file{'/var/run/eosd': #/var/run/eosd is still used on eos-fusex-core rpm...
    ensure => directory,
    mode   => '0755',
    owner  => root,
    group  => root,
  }
  -> file{'/var/run/eosd/credentials': #/var/run/eosd/credentials is still used on eos-fusex-core rpm...
    ensure => directory,
    mode   => '1777',
    owner  => root,
    group  => root,
  }
  -> file{'/var/run/eosd/credentials/store': #/var/run/eosd/credentials/store is still used on eos-fusex-core rpm...
    ensure => directory,
    mode   => '1777',
    owner  => root,
    group  => root,
  }
  -> file{'/var/log/eos':
    ensure => directory,
    mode   => '0755',
    owner  => daemon,
    group  => daemon,
  }
  -> file{'/var/log/eos/fuse':
    ensure => directory,
    mode   => '0755',
    owner  => daemon,
    group  => daemon,
  }
  -> file{'/etc/eos':
    ensure => directory,
    mode   => '0755',
    owner  => root,
    group  => root,
  }

  $eosmounts.each |$_mount | {
    # Has to be oneliner for this, can't get slice or filter to do the correct thing.
    # Want to delete everything except for 'conf'
    $slice = { 'conf' => $eosclient::_deprecated_eosclient_custom_config['conf'] }
    if $_mount in $allmounts {
      $_instanceconfig = deep_merge($slice,
                                    $eosclient::_deprecated_eosclient_instance_config[$_mount],$eosclient::instance_config[$_mount])
    } else{
      fail("There is no default configuration for mount ${_mount}")
    }

    $_config = deep_merge($eosclient::common_config,$_instanceconfig)

    eosclient::mount{$_mount:
      config  => $_config,
      require => File['/etc/eos'],
    }
  }

  file {'/usr/local/sbin/eos-cleanup.sh':
    ensure => bool2str($eosclient::enable_clean,'file','absent'),
    source => 'puppet:///modules/eosclient/eos-cleanup.sh',
    mode   => '0755',
    owner  => root,
    group  => root,
  }

  if $eosclient::enable_clean {
    cron {'eos-cleanup':
      command => '/usr/local/sbin/eos-cleanup.sh &> /dev/null',
      minute  => '*/5',
      require => File['/usr/local/sbin/eos-cleanup.sh'],
    }
  }

  if $eosclient::enable_autofs {

    include eosclient::autofs
    Eosclient::Mount[$eosmounts] -> Package['autofs']

  }
}
