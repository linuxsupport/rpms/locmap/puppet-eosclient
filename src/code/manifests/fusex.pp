class eosclient::fusex {

  # repo set up etc.
  # keep this include until all clients drop the deprecated include eosclient::fuse syntax
  include eosclient

  #################################
  # Cleanup for old configuration #
  #################################

  # Rpms will install new configurations, so we clean the current ones.

  if empty($eosclient::mounts) {
    $eosmounts = $eosclient::default_mounts
  } else {
    $eosmounts = $eosclient::mounts
  }

  eosclient::mount { $eosmounts:
    remove => true,
  }

  # This files should not be there with this configuration
  $files_to_clean=['/etc/auto.eos', '/etc/auto.master.d/eos.autofs']
  file{ $files_to_clean:
    ensure => absent,
    notify => Service['autofs'],
  }

  # orderly removal of sysvinit versions of eos-fuse. We are using autofs so these are not needed.
  stdlib::ensure_packages('eos-fuse', {'ensure' => 'absent'})
  stdlib::ensure_packages('eos-fuse-sysv', {'ensure' => 'absent'})

  ###################
  # Folder creation #
  ###################

  file{'/eos':
    ensure  => directory,
    mode    => '0755',
    owner   => root,
    group   => root,
    seltype => 'default_t',
  }
  -> file{'/var/log/eos':
    ensure => directory,
    mode   => '0755',
    owner  => daemon,
    group  => daemon,
  }
  -> file{'/var/log/eos/fusex':
    ensure => directory,
    mode   => '0755',
    owner  => daemon,
    group  => daemon,
  }
  -> file{'/etc/eos':
    ensure => directory,
    mode   => '0755',
    owner  => root,
    group  => root,
  }

  ###############################
  # eos client packages install #
  ###############################

  $eospkg = ['eos-fusex']

  Osrepos::Kojitag['eos'] -> Package[$eospkg]

  stdlib::ensure_packages($eospkg, {'ensure' => $eosclient::version})

  ########################
  # autofs configuration #
  ########################

  Package{'cern-eos-autofs-lxplus':
    ensure  => $eosclient::config_version,
    notify  => Service['autofs'],
    require => Osrepos::Kojitag['eos'],
  }

  if $eosclient::enable_squashfs {
    Package { 'cern-eos-autofs-squashfs':
      ensure  => $eosclient::config_version,
      notify  => Service['autofs'],
      require => Osrepos::Kojitag['eos'],
    }
  }

  ensure_resource('service','autofs',
    { ensure     => true,
      enable     => true,
      hasrestart => true,
    }
  )

  ##########################
  # Logrotate configuration
  ##########################

  $logrotate_fusex_changes = $eosclient::fuse_logrotate_size ? {
    Undef   =>  [
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate ${eosclient::fuse_logrotate_days}",
                  'rm /files/etc/logrotate.d/eos-fusex-logs/rule/size',
                ],
    default => [
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/rotate ${eosclient::fuse_logrotate_days}",
                  "set /files/etc/logrotate.d/eos-fusex-logs/rule/size ${eosclient::fuse_logrotate_size}",
                ],
  }

  augeas{'eosfusex_logrotate':
    changes => $logrotate_fusex_changes,
    incl    => '/etc/logrotate.d/eos-fusex-logs',
    lens    => 'Logrotate.lns',
    require =>  Package[$eospkg],
  }

  ############################
  # Manage eos-cleanup.sh cron
  ############################

  file {'/usr/local/sbin/eos-cleanup.sh':
    ensure => bool2str($eosclient::enable_clean,'file','absent'),
    source => 'puppet:///modules/eosclient/eos-cleanup.sh',
    mode   => '0755',
    owner  => root,
    group  => root,
  }

  if $eosclient::enable_clean {
    cron {'eos-cleanup':
      command => '/usr/local/sbin/eos-cleanup.sh &> /dev/null',
      minute  => '*/5',
      require => File['/usr/local/sbin/eos-cleanup.sh'],
    }
  }

}
