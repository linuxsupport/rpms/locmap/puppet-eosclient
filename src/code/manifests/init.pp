# @summary configures an EOS client, optionally FUSE mount /eos
#
# @param instance_config
#  dict with configuration info for each EOS instance the module knows about
# @param common_config
#  dict with configuration info that applies to all EOS instances
# @param version
#  hardcoded EOS RPM version to install
# @param enable_fuse
#  whether to enable FUSE mounts (default: just install CLI)
# @param enable_pam_hook
#  whether to hook up `eosfusebind` into the login sequence (via PAM),
#  this is mostly needed for old EOS-FUSE=eosd
# @param enable_autofs
#  use the automounter for EOS mounts (this is the only supported config)
# @param mounts
#  which EOS instances to mount on this machine.
#  Each entry here must have a corresponding configuration dict
# @param default_mounts
# (internal, do not use: what to mount if above `mounts` parameter is not set)
# @param yum_priority
#  numeric YUM repository priority
# @param manage_autofs_service
#  should this module configure the automounter
# @param manage_autofs_rpm
#  should this module install the automounter
# @param fuse_logrotate_days
#  how many days of FUSE logs to keep
# @param fuse_logrotate_size
#  how big can FUSE logs become before being rotated
# @param enable_lemon
#  (obsolete, to be dropped)
# @param bindmounts
#  use Linux bindmounts to have several (e.g per-letter) areas from the same EOS instance mounted together
# @param configure_via_rpm
#  install FUSEX config from RPM, ignore puppet configuration for mountpoints
# @param config_version
#  hardcode the FUSEX config RPM version
# @param enable_squashfs
#  install cern-eos-autofs-squashfs - see code/README.md for details
# @param use_osrepos
#  allows to _not_ rely on the 'eos' KOJI repository
# @param enable_clean
#  If true install and run eos_cleanup.sh script

class eosclient (
  Hash $instance_config,
  Hash $common_config,
  String[1] $version                       = 'present',
  Boolean $enable_fuse                     = false,
  Boolean $enable_pam_hook                 = false,
  Boolean $enable_autofs                   = true,
  Array[String[1]] $mounts                 = [],
  Array[String[1]] $default_mounts         = [],
  Integer $yum_priority                    = 9,
  Boolean $manage_autofs_service           = true,
  Boolean $manage_autofs_rpm               = true,
  Integer $fuse_logrotate_days             = 7,
  Optional[String[1]] $fuse_logrotate_size = undef,
  Boolean $enable_lemon                    = false,
  Hash $bindmounts                         = {},
  Boolean $configure_via_rpm               = false,
  String[1] $config_version                = 'present',
  Boolean $enable_squashfs                 = false,
  Boolean $use_osrepos                     = true,
  Boolean $enable_clean                    = true,
) {

  $_deprecated_eosclient_instance_config = lookup('eosclient_instance_config',Hash,'deep',{})
  unless empty($_deprecated_eosclient_instance_config) {
    deprecation('eosclient_instance_config','The hiera key eosclient_instance_config is deprecated, use eosclient::instance_config instead..')
  }
  $_deprecated_eosclient_custom_config   = lookup('eosclient_custom_config',Hash,'deep',{})
  unless empty($_deprecated_eosclient_custom_config) {
    deprecation('eosclient_custom_config','The hiera key eosclient_custom_config is deprecated, use eosclient::instance_config instead..')
  }

  if $facts['os']['release']['major'] == '7' and $facts['os']['architecture'] != 'x86_64' {
    fail ('eosclient supports only x86_64 on el7')
  } elsif  $facts['os']['architecture'] != 'x86_64' and $facts['os']['architecture'] != 'aarch64' {
    fail ('eosclient supports only x86_64 or aarch64 on el8 and newer')
  }

  stdlib::ensure_packages(['jemalloc'])

  if $use_osrepos {
    osrepos::kojitag { 'eos':
      available_major_versions => [7, 8, 9],
      description              => 'EOS binaries from CERN Koji',
      priority                 => $yum_priority,
      enable_debuginfo         => 1,
      testing_enable_variable  => 'osrepos_eos_testing_enable',
    }
    $package_requires = Osrepos::Kojitag['eos']
  } else {
    $package_requires = undef
  }

  stdlib::ensure_packages (['eos-client'], {
    'ensure'  => $version,
    'require' => $package_requires,
  })
  stdlib::ensure_packages (['eos-debuginfo'], {
    'ensure'  => 'absent',
  })
  # the extra libraries in /opt are only on CC7 and newer, only need debuginfo here
  stdlib::ensure_packages (['eos-xrootd-debuginfo'], {
    'require' => $package_requires,
  })

  if $configure_via_rpm {
    unless empty($eosclient::mounts) {
      warning('With eosclient::configure_via_rpm, this setting is ignored. Autofs will mount any instance on demand.')
    }
    unless empty($eosclient::common_config) {
      warning('With eosclient::configure_via_rpm, custom config is ignored.')
    }
    unless empty($eosclient::instance_config) {
      warning('With eosclient::configure_via_rpm, custom config is ignored.')
    }
    include eosclient::fusex
  } else {
    if $enable_fuse {
      include eosclient::fuse
    }
    if $enable_pam_hook {
      include eosclient::fuse::interactive
    }
  }

  # locmap additions
  $_suffix = $facts['os']['name'] ? {
    'AlmaLinux' => 'al',
    'RedHat'    => $facts['os']['release']['major'] ? {
      '7'     => '',
      default => 'el'
    },
    default     => '',
  }

  Yumrepo <| |> -> Package <| provider != 'rpm' |>
  yumrepo { "eos${facts['os']['release']['major']}-desktop":
    descr    => "eos${facts['os']['release']['major']}-desktop",
    baseurl  => "http://linuxsoft.cern.ch/internal/repos/eos${facts['os']['release']['major']}${_suffix}-desktop/${facts['os']['architecture']}/os",
    gpgcheck => false,
    enabled  => 1,
    before   => Package['eos-client'],
  }
  yumrepo { "eos${facts['os']['release']['major']}-desktop-debuginfo":
    descr    => "eos${facts['os']['release']['major']}-desktop-debuginfo",
    baseurl  => "http://linuxsoft.cern.ch/internal/repos/eos${facts['os']['release']['major']}${_suffix}-desktop/${facts['os']['architecture']}/debug",
    gpgcheck => false,
    enabled  => 1,
    before   => Package['eos-client'],
  }
}
