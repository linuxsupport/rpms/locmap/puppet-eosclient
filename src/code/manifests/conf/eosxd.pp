# @summary
#  writes per-mountpoint JSON config file for FUSEX
#  will be instantiated several times (once per mountpoint)
#
# @param config
#  Hiera dict with at least hostport:, remotemountdir:
# @param instance
#  client-local name of the mountpoint
define eosclient::conf::eosxd (
  Hash $config,
  String[1] $instance = $name,
) {

  $_json = stdlib::to_json($config['conf'])
  file { "/etc/eos/fuse.${instance}.conf":
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "${_json}\n",
    require => [File['/etc/eos'], Package['eos-fusex']],
  }
}
