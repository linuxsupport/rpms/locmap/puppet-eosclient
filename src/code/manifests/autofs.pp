class eosclient::autofs () inherits eosclient {

  if $eosclient::manage_autofs_rpm {
    stdlib::ensure_packages('autofs')
  }

  if empty($eosclient::mounts) {
    $eosmounts = $eosclient::default_mounts
  } else {
    $eosmounts = $eosclient::mounts
  }

  # since 2022-07 we only use FUSEX to mount EOS

  # any 'bind-mounted' instances? (used in ERB)
  $eosclient_bindmounts = $eosclient::bindmounts

  $_autofs_notify = $eosclient::manage_autofs_service ? {
    true    => 'Service[autofs]',
    default => undef ,
  }

  # orderly removal of sysvinit versions of eos-fuse
  stdlib::ensure_packages('eos-fuse', {'ensure' => 'absent'})
  stdlib::ensure_packages('eos-fuse-sysv', {'ensure' => 'absent'})

  ini_setting { 'set autofs browse mode yes':
    ensure  => present,
    path    => '/etc/autofs.conf',
    section => ' autofs ',
    setting => 'browse_mode',
    value   => 'yes',
    notify  => $_autofs_notify,
    require =>  Package['autofs', 'eos-fuse', 'eos-fuse-sysv'],
  }
  -> file{'/etc/auto.eos': # add line for each instance to auto.eos: <instance> -fstype=eos :user
    content => template('eosclient/auto.eos.erb'),
    mode    => '0644',
    owner   => root,
    group   => root,
    notify  => $_autofs_notify,
  }

  # use auto.master.d directory on C8 and newer.
  if versioncmp($facts['os']['release']['major'],'7') <= 0 {
    augeas{'eos_automaster': # add line to auto.master: /eos /etc/auto.eos
      context => '/files/etc/auto.master/',
      lens    => 'Automaster.lns',
      incl    => '/etc/auto.master',
      changes => [
        'set 01      /eos',
        'set 01/map  /etc/auto.eos',
      ],
      onlyif  => 'match *[map="/etc/auto.eos"] size == 0',
      require => File['/etc/auto.eos'],
      notify  => $_autofs_notify,
    }
  } else {
    file{'/etc/auto.master.d/eos.autofs':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => "#Puppet Maintained\n/eos /etc/auto.eos\n",
      require => File['/etc/auto.eos'],
      notify  => $_autofs_notify,
    }
  }

  if $eosclient::manage_autofs_service {
    ensure_resource('service','autofs',
      { ensure     => true,
        enable     => true,
        hasrestart => true,
      }
    )
  }
}
