class eosclient::fuse::interactive {
  include eosclient::fuse
  # PAM config for SSH access
  # unfortunaetly the augeas 'pam' thing does not seem to match ever.
  exec {'EOSFUSE at SSH login':
    command => "/bin/sed -i -e '/session \\+required \\+pam_selinux.so open env_params/a session    optional     pam_exec.so  debug seteuid /usr/bin/eosfusebind --pam' /etc/pam.d/sshd",
    unless  => '/bin/grep -q /usr/bin/eosfusebind /etc/pam.d/sshd',
  }
  # try to get all of this in place before SSH runs - if it is configured to do so?
  Package['eos-client','eos-fusex'] -> Exec['EOSFUSE at SSH login']
  if defined(Service['sshd']) and defined(Package['sshd']) {  # icky evaluation order dependance, but better than an error or no dep at all
    Package['openssh-server'] -> Exec['EOSFUSE at SSH login'] -> Service['sshd']
  }

  # auto-guess EOS_MGM_URL based on group
  file {'/etc/profile.d/eos-select.sh':
    source => 'puppet:///modules/eosclient/eos-select.sh',
    mode   => '0444',
    owner  => root,
    group  => root,
  }
  file {'/etc/profile.d/eos-select.csh':
    source => 'puppet:///modules/eosclient/eos-select.csh',
    mode   => '0444',
    owner  => root,
    group  => root,
  }
}
