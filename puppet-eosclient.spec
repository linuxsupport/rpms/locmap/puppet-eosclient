Name:		puppet-eosclient
Version:        2.13
Release:	1%{?dist}
Summary:	Puppet module for configuring eos file system

Group:		CERN/Utilities
License:	BSD
URL:		http://linux.cern.ch
Source0:	%{name}-%{version}.tgz

BuildArch:  noarch

Requires:   puppet
Requires:   puppet-stdlib
Requires:   puppet-inifile
Requires:   logrotate
Requires:   epel-release

%description
Puppet module for configuring eos file system.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/eosclient
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/eosclient/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/eosclient/
cp  data/eosclient.yaml %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/eosclient/eosclient.yaml
touch %{buildroot}/%{_datadir}/puppet/modules/eosclient/linuxsupport

%files -n puppet-eosclient
/etc/locmap/code/environments/production/hieradata/modules/eosclient/eosclient.yaml
%{_datadir}/puppet/modules/eosclient
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 15 2024 Ben Morrice <ben.morrice@cern.ch> 2.13-1
- Simplify deployment of hieradata

* Tue Oct 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 2.12-1
- Rebased to #a911be30 by locmap-updater

* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.11-1
- Add autoreconfigure %post script

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> 2.10-1
- Rebase to #975a9ce7fb180a9fdf7f1e339977403efe9bb4f0

* Fri Mar 03 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> 2.9-1
- Add support to aarch64 nodes

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.8-2
- Bump release for disttag change

* Thu Oct 27 2022 Ben Morrice <ben.morrice@cern.ch> - 2.8-1
- Rebase on it-puppet-module-eosclient commit #8a3aded6

* Mon Feb 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.7-2
- fix module dependencies

* Thu Feb 11 2021 Ben Morrice <ben.morrice@cern.ch> - 2.7-1
- move away from hiera lookups

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> - 2.6-1
- Build for el8

* Tue Jul 14 2020 Ben Morrice <ben.morrice@cern.ch> - 2.5-1
- do not install debuginfo packages

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.4-4
 - Build for el8

* Wed Dec 04 2019 Ben Morrice <ben.morrice@cern.ch> - 2.4-3
 - Add epel-release as a Requires, as eos now depends on epel
 
* Fri Jul 26 2019 Ben Morrice <ben.morrice@cern.ch> - 2.4-2
- RQF1360232: Add koji eos7-desktop repo to code/manifests.pp

* Fri Jul 19 2019 Ben Morrice <ben.morrice@cern.ch> - 2.4-1
- Rebase on it-puppet-module-eosclient commit 20949881

* Mon Nov 26 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.3-2
- Rebase on it-puppet-module-eosclient commit d8342e47

* Mon Jul 09 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.1-1
- Rebase on it-puppet-module-eosclient commit #26e9852

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebase on it-puppet-module-eosclient commit #db76af58

* Mon Oct 09 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.5-2
- Rebase on it-puppet-module-eosclient commit #dfbb8b3a
- Tested with eos fuse 4.1.30

* Tue Mar 07 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.4-3
- Rebase on it-puppet-module-eosclient commit #6248ee10
- Add additional mounts point based on it-puppet-module-bagplus
- Cleanup some instance config

* Fri Dec 09 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-2
- Rebase on it-puppet-module-eosclient commit #2b67c9d6

* Fri Dec 09 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-1
- Rebase on it-puppet-module-eosclient commit efdddc6a

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Tue Jul 5 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
